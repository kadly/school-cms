class CreateNovations < ActiveRecord::Migration
  def change
    create_table :novations do |t|
      t.string :heading
      t.text :subject
      t.references :page
      t.timestamps null: false
    end
  end
end

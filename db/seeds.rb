# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
p1 = Page.create(title: "Beaches", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")
Article.create(description: "Ipanema", item: "The beach of Ipanema is known for its elegant development and its social life.", page_id: p1.id)
Article.create(description: "7 Mile Beach", item: "The western coastline contains the island's finest beaches.", page_id: p1.id)
Article.create(description: "El Castillo", item: "An elite destination famous for its white sand beaches",  page_id: p1.id)

p2 = Page.create(title: "History", content: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.")

p3 = Page.create(title: "Novation", content: "content")
Novation.create(heading: "Dolomites", subject: "The Dolomites are a mountain range located in northeastern Italy famous for skiing in the winter months.", page_id: p3.id)
Novation.create(heading: "Chamonix", subject: "It was the site of the first Winter Olympics in 1924", page_id: p3.id)
Novation.create(heading: "Vail", subject: "The second largest single mountain ski resort in the United States", page_id: p3.id)
class Article < ActiveRecord::Base
  belongs_to :page, touch: true

  validates :item, presence: true
  validates :description, presence: true
  validates :page_id, presence: true
end

class Novation < ActiveRecord::Base
  belongs_to :page

  validates :heading, presence: true
  validates :subject, presence: true
  validates :page_id, presence: true
end

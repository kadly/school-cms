class ArticlesController < ApplicationController
  def index
    @article = Article.all
  end

  def new
    @page = Page.find(params[:page_id])
    @page.articles.new
  end

  def edit
    @article = Article.find(params[:id])
  end

  def create
    @page = Page.find(params[:page_id])
    @article = @page.articles.create(article_params)
    #@article = Article.new(article_params)
    if @article.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def last_update
    @a= @article.last.updated_at.strftime("%d.%m.%Y")
  end

  def update
    @article = Article.find(params[:id])
    if @article.update_attributes(article_params)
      #redirect_to(:action => 'show', :id => @article.id)
      redirect_to root_path
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
    redirect_to root_path
  end

  private
    def article_params
      params.require(:article).permit(:item, :description)
    end
end

class PagesController < ApplicationController
  def index
    @pages = Page.all.order("title ASC")
  end

  def show
    @page = Page.find(params[:id])
    @articles = @page.articles
    @novations = @page.novations
    @pages = Page.all.order("title ASC")
    @novations = Novation.where('created_at > ?', 1.year.ago).order("created_at DESC") if @page.title == "Novations"
    @current_year = Time.now.year
    @today = Time.now.strftime("%d.%m.%Y")
    @articles = Article.all if @page.title == "Articles"
  end
  
  def new
  end

  def create
    @page = Page.new(page_params)
    if @page.save
      redirect_to root_path
    else
      render 'new'
    end
  end

  def edit
    @page = Page.find(params[:id])
  end

  def update
    @page = Page.find(params[:id])
    if @page.update_attributes(page_params)
      redirect_to Page.find(params[:id])
    else
      render 'edit'
    end
  end

  def destroy
    @page = Page.find(params[:id])
    @page.destroy
    redirect_to root_path
  end

  private
    def page_params
      params.require(:page).permit(:title, :content)
    end
end

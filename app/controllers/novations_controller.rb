class NovationsController < ApplicationController
  def index
    @novations = Novation.all.order("created_at DESC")
  end

  def show
    @novation = Novation.find(params[:id])
  end
  
  def new
    @novation = Novation.new
  end

  def create
    @novation = Novation.new(novation_params)
    if @novation.save
      redirect_to '/pages/3'
      #redirect_to :controller => 'pages', :action => 'show', :id => 1
    else
      render 'new'
    end
  end

  def edit
    @novation = Novation.find(params[:id])
  end

  def update
    @novation = Novation.find(params[:id])
    if @novation.update_attributes(novation_params)
      #redirect_to(:action => 'show', :id => @article.id)
      redirect_to '/pages/3'
    else
      render 'edit'
    end
  end

  def destroy
    @novation = Novation.find(params[:id])
    @novation.destroy
    redirect_to '/pages/3'
  end

  def archives
    @novations = Novation.order("created_at DESC")
  end

  private
    def novation_params
      params.require(:novation).permit(:heading, :subject, :page_id)
    end
end
